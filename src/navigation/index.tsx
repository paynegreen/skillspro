// In App.js in a new project

import * as React from 'react';
import {View, Text, Alert} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Login';
import Register from '../screens/Register';
import AsyncStorage from '@react-native-community/async-storage';
import Home from '../screens/Home';
import Request from '../screens/Request';
import History from '../screens/History';
import {Root} from 'native-base';

const Stack = createStackNavigator();

function App() {
  const [state, setState] = React.useState({
    auth: null,
    user: null,
  });

  const getUserDetails = async () => {
    try {
      const details = await AsyncStorage.getItem('@user');
      const parseData = JSON.parse(details);

      if (parseData.auth) {
        setState(parseData);
      }
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  React.useEffect(() => {
    getUserDetails();
  }, []);

  return (
    <Root>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Request" component={Request} />
          <Stack.Screen name="History" component={History} />
        </Stack.Navigator>
      </NavigationContainer>
    </Root>
  );
}

export default App;
