import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Container, Text, Content, Button} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

const Home = (props) => {
  const navigateToScreen = (screen: string) => {
    const {navigate} = props.navigation;

    navigate(screen);
  };

  const logOut = async () => {
    props.navigation.goBack();
  };

  return (
    <Container>
      <Content>
        <View style={styles.container}>
          <View style={styles.noticeContainer}>
            <Text style={{fontSize: 13}}>
              Please note all service requests attract a processing fee of GHS
              5.00.
            </Text>
          </View>
          <Button
            style={[styles.button, styles.requestBtn]}
            block
            onPress={() => navigateToScreen('Request')}>
            <Text>New Request</Text>
          </Button>
          <Button
            style={[styles.button, styles.historyBtn]}
            block
            onPress={() => navigateToScreen('History')}>
            <Text>Request History</Text>
          </Button>
          <Button block style={[styles.logOut]} onPress={logOut}>
            <Text style={styles.logOutText}>Sign Out</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  button: {
    marginVertical: 10,
  },
  requestBtn: {
    backgroundColor: 'green',
  },
  historyBtn: {
    backgroundColor: 'grey',
  },
  noticeContainer: {
    marginVertical: 10,
    padding: 10,
    borderRadius: 5,
    backgroundColor: 'lightblue',
  },
  logOut: {
    marginVertical: 10,
    backgroundColor: 'transparent',
  },
  logOutText: {
    fontSize: 13,
    textDecorationLine: 'underline',
    color: 'black',
  },
});

export default Home;
