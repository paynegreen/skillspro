import React from 'react';
import {View, StyleSheet, FlatList, Image} from 'react-native';
import {Container, Content, Text} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

const renderItem = ({item, index, separators}) => {
  return (
    <View style={{marginBottom: 10}}>
      <Text>Category: {item.category}</Text>
      <Text>Description: {item.description}</Text>
      {/* <Text>Image:</Text>
      <Image source={item.item} style={styles.image} /> */}
    </View>
  );
};

const History = () => {
  const [data, setData] = React.useState([]);

  const getData = async () => {
    try {
      const data = await AsyncStorage.getItem('@data');
      const parseData = JSON.parse(data);

      setData(parseData);
      console.log(parseData);
    } catch (error) {
      console.log(error.message);
    }
  };

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <Container>
      {/* <Content> */}
      <View style={styles.container}>
        <FlatList
          data={data}
          keyExtractor={(item: object, index: number) => `${index}`}
          renderItem={renderItem}
          ListEmptyComponent={() => <Text>No requests made yet</Text>}
        />
      </View>
      {/* </Content> */}
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  image: {
    height: 200,
    width: 200,
  },
});

export default History;
