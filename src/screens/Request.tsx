import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {
  Container,
  Content,
  Text,
  Input,
  Item,
  Label,
  Button,
  Picker,
  Toast,
  Icon,
} from 'native-base';
import ImagePicker from 'react-native-image-picker';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

const options = {
  title: 'Image of issue',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const Request = (props) => {
  const [image, setImage] = React.useState(null);
  const [category, setCategory] = React.useState('Electricals');
  const [description, setDescription] = React.useState('');
  const [data, setData] = React.useState([]);

  const addImage = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        setImage(source);
      }
    });
  };

  const getData = async () => {
    try {
      const data = await AsyncStorage.getItem('@data');
      const parseData = JSON.parse(data);

      setData(parseData);
    } catch (error) {
      console.log(error.message);
    }
  };

  const saveRequest = () => {
    const payload = {
      category,
      description,
      image,
    };

    data.push(payload);

    console.log(data);

    AsyncStorage.setItem('@data', JSON.stringify(data));

    Toast.show({
      text: 'Request submitted successfully',
      type: 'success',
    });

    props.navigation.goBack();
  };

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <Container>
      <Content>
        <View style={stylse.container}>
          <Text>Complete the form to submit a service request.</Text>
          <View style={stylse.form}>
            <Item style={stylse.inputContainer} picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{width: '100%'}}
                placeholder="Choose a category. E.g. Electricals"
                placeholderIconColor="#007aff"
                selectedValue={category}
                onValueChange={setCategory}>
                <Picker.Item label="Electricals" value="Electricals" />
                <Picker.Item label="Furniture" value="Furniture" />
                <Picker.Item label="Automobile" value="Automobile" />
                <Picker.Item label="Landscaping" value="Landscaping" />
              </Picker>
            </Item>
            <Item stackedLabel>
              <Label>Description</Label>
              <Input
                placeholder="e.g. Kitchen sink tap not working"
                style={stylse.input}
                multiline
                onChangeText={(description) => setDescription(description)}
              />
            </Item>
            <TouchableOpacity onPress={addImage}>
              <View style={{marginTop: 10}}>
                {image === null ? (
                  <Text style={{fontSize: 15, color: 'rgb(87,87,87)'}}>
                    Tap to add image of issue
                  </Text>
                ) : (
                  <Image style={stylse.avatar} source={image} />
                )}
              </View>
            </TouchableOpacity>
          </View>
          <Button block onPress={saveRequest}>
            <Text>Submit</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

const stylse = StyleSheet.create({
  container: {
    marginVertical: 20,
    paddingHorizontal: 20,
  },
  form: {
    marginTop: 10,
    marginBottom: 20,
  },
  input: {
    fontSize: 13,
  },
  inputContainer: {
    marginVertical: 10,
  },
  avatar: {
    height: 200,
    width: 200,
    resizeMode: 'contain',
  },
});

export default Request;
