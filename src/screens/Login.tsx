import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {
  Container,
  Input,
  Form,
  Text,
  Item,
  Header,
  Button,
  Label,
  Content,
  Toast,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';

const {height, width} = Dimensions.get('window');

const Login = ({navigation}) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  const navigateToRegister = () => {
    navigation.navigate('Register');
  };

  const login = async () => {
    const data = await AsyncStorage.getItem('@user');
    const parseData = JSON.parse(data);

    if (email !== parseData.user.email) {
      Toast.show({text: 'Invalid email or password', type: 'danger'});
      return;
    }

    Toast.show({text: 'Login successful', type: 'success'});
    navigation.navigate('Home');
  };

  return (
    <Container>
      <Content>
        <View style={styles.container}>
          <Text style={styles.headerText}>Welcome to Skills Pro</Text>
          <Text>Sign in to continue</Text>
          <View style={styles.inputContainer}>
            <Item stackedLabel>
              <Label>Email</Label>
              <Input
                placeholder="john@example.com"
                style={styles.input}
                keyboardType="email-address"
                autoCapitalize="none"
                onChangeText={(email) => setEmail(email)}
              />
            </Item>
            <View style={{marginVertical: 10}} />
            <Item stackedLabel>
              <Label>Password</Label>
              <Input
                placeholder="********"
                style={styles.input}
                secureTextEntry
              />
            </Item>
          </View>
          <Button block onPress={login}>
            <Text style={styles.btnText}>Sign In</Text>
          </Button>
          <Button
            block
            style={[styles.noBackground, styles.registerBtn]}
            onPress={navigateToRegister}>
            <Text style={[styles.textColor]}>
              Don't have an account?{' '}
              <Text style={styles.registerText}>Sign Up</Text>
            </Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    marginVertical: width / 2,
    alignItems: 'center',
  },
  inputContainer: {
    width: '100%',
    marginVertical: width / 10,
  },
  headerText: {
    fontSize: 25,
    color: 'grey',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  input: {
    color: 'black',
  },
  btnText: {
    fontWeight: 'bold',
  },
  registerText: {
    fontSize: 14,
    textDecorationLine: 'underline',
  },
  textColor: {
    fontSize: 14,
    color: 'black',
  },
  noBackground: {
    backgroundColor: 'transparent',
  },
  registerBtn: {
    marginVertical: 10,
  },
});

export default Login;
